package com.mobilejazz.kotlin.core.ui.base.view

interface MVPView {

  fun onDisplayError(throwable: Throwable)

}
